const sumar = require("../index");

const assert =  require("assert");

//  Afirmación : Afirmar que pase algo, esperando que sea cierto.
describe("Probar la suma de dos numeros", ()=>{

    // Afirmar que cinco más cinco es igual a diez
    it('Cinco más cinco es diez', ()=> {
        assert.strictEqual(10, sumar(5,5))
    });

    // Afirmar que cinco más cinco no es igual a cincuenta y cinco
    it("Cinco más cinco no es cincuenta y cinco", ()=>{
        assert.notStrictEqual("55", sumar(5,5))
    });

});